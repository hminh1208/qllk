<?php
/*
.------------------
.Language: English
.------------------
.*/
 
$lang = array();
 
//LOGIN PAGE
$lang['LOGIN_PAGE_TITLE'] = 'Please Sign In';
$lang['EMAIL_PLACEHOLDER'] = 'E-mail';
$lang['PASSWORD_PLACEHOLDER'] = 'Password';
$lang['REMEMBER_ME'] = 'Remember Me';
$lang['LOGIN_BUTTON'] = 'Login';