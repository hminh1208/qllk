<?php
/*
.------------------
.Language: English
.------------------
.*/
 
$lang = array();
$js_code = 'vi_VN';
//LOGIN PAGE
$lang['LOGIN_PAGE_TITLE'] = 'Xin mời đăng nhập';
$lang['EMAIL_PLACEHOLDER'] = 'Thư điện tử';
$lang['PASSWORD_PLACEHOLDER'] = 'Mật khẩu';
$lang['REMEMBER_ME'] = 'Lưu mật khẩu';
$lang['LOGIN_BUTTON'] = 'Đăng nhập';
$lang['SIGNUP_BUTTON'] = 'Đăng ký';
$lang['FORGOTPASS_BUTTON'] = 'Lấy lại mật khẩu';