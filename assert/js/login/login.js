

$(document).ready(function() {
    $('.loginForm').bootstrapValidator({
        //message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email: {
                validators: {
                   
                    notEmpty: {
                        //message: 'The email is required and cannot be empty'
                    },
                    emailAddress: {
                        //message: 'The input is not a valid email address'
                    }
                }
            },
            password: {
                validators: {
                  
                    notEmpty: {
                        //message: 'The email is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6
                        //message: 'The username must be more than 6 and less than 30 characters long'
                    }
                }
              
            }
        }
    });
});

$(document).ready(function() {
    $('.loginForm').bootstrapValidator();
});

function checkLogin(){
    
    var data = {
      "action": "login",
      "email": $('#email').val(),
      "password": $('#password').val(),
      "remember": $('#remember').prop('checked')
    };
    data = $(this).serialize() + "&" + $.param(data);
    $.ajax({
      type: "POST",
      dataType: "json",
      url: "../script/authentication.php", //Relative or absolute path to response.php file
      data: data,
      success: function(data) {
        if(data["valid"]){
            alert('true');
        }else{
            alert('false');
        }
      }
    });
    return false;

};