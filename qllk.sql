-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2015 at 09:55 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `qllk`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`) VALUES
(1, 'Mainboard', NULL),
(2, 'RAM - Bộ nhớ trong', NULL),
(3, 'HDD - Ổ đĩa cứng', NULL),
(4, 'SSD - Ổ cứng thể rắn', NULL),
(5, 'VGA - Card màn hình', NULL),
(6, 'Keyboard - Bàn phím', NULL),
(7, 'MOUSE - Chuột', NULL),
(8, 'PSU - Nguồn máy tính', NULL),
(9, 'LCD - Màn hình', NULL),
(10, 'PRINTER - Máy in', NULL),
(11, 'SPEAKER - Loa', NULL),
(12, 'OTHER - Phụ kiện', NULL),
(13, 'Case - Vỏ máy tính', NULL),
(14, 'Ổ cứng gắn ngoài', 3),
(15, 'Ổ cứng gắn trong', 3),
(16, 'Chuột thường', 7),
(17, 'Chuột gaming', 7),
(18, 'Lót chuột', 7),
(19, 'Headphone', 11),
(20, 'Loa để bàn', 11),
(21, 'Laptop', 2),
(22, 'Desktop', 2);

-- --------------------------------------------------------

--
-- Table structure for table `cate_manu`
--

CREATE TABLE IF NOT EXISTS `cate_manu` (
  `category` int(10) unsigned NOT NULL,
  `manufacturer` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cate_manu`
--

INSERT INTO `cate_manu` (`category`, `manufacturer`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(22, 5),
(21, 6),
(22, 7);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `address`, `phone`, `email`) VALUES
(1, 'Nguyễn Văn A', '1 Nguyễn Văn Linh', '1111111111', 'nguyenvana@gmail.com'),
(2, 'Trần Văn B', '2 Nguyễn Văn Linh', '2222222222', 'tranvanb@gmail.com'),
(3, 'Phan Thị C', '3 Nguyễn Văn Linh', '3333333333', 'phanthic@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `imports`
--

CREATE TABLE IF NOT EXISTS `imports` (
`id` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `time` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `imports`
--

INSERT INTO `imports` (`id`, `user`, `time`) VALUES
(1, 3, '2014-10-17'),
(2, 3, '2014-10-15'),
(3, 4, '2014-10-15'),
(4, 2, '2014-10-19'),
(7, 1, '2014-10-28');

-- --------------------------------------------------------

--
-- Table structure for table `imports_details`
--

CREATE TABLE IF NOT EXISTS `imports_details` (
`id` int(10) unsigned NOT NULL,
  `import` int(10) unsigned NOT NULL,
  `product` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `imports_details`
--

INSERT INTO `imports_details` (`id`, `import`, `product`, `quantity`) VALUES
(1, 1, 1154347624, 3),
(2, 1, 1154362778, 2),
(3, 1, 1163741821, 2),
(4, 2, 1676634727, 2),
(5, 2, 1726366814, 2),
(6, 3, 1833816152, 2),
(7, 3, 1833816152, 2),
(8, 3, 1676634727, 2),
(9, 1, 1285367113, 3),
(10, 7, 1285367113, 2);

--
-- Triggers `imports_details`
--
DELIMITER //
CREATE TRIGGER `imports_details_after_delete_update_product` AFTER DELETE ON `imports_details`
 FOR EACH ROW UPDATE `products` SET `products`.`quantity` = `products`.`quantity` - OLD.`quantity` WHERE `products`.`id` = OLD.`product`
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `imports_details_after_insert_update_products` AFTER INSERT ON `imports_details`
 FOR EACH ROW UPDATE `products` SET `products`.`quantity` = `products`.`quantity` + NEW.`quantity` WHERE `products`.`id` = NEW.`product`
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `imports_details_after_update_update_products` AFTER UPDATE ON `imports_details`
 FOR EACH ROW UPDATE `products` SET `products`.`quantity` = `products`.`quantity` - OLD.`quantity` + NEW.`quantity` WHERE `products`.`id` = NEW.`product`
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE IF NOT EXISTS `manufacturers` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `name`) VALUES
(1, 'Asus'),
(2, 'Gigabyte'),
(7, 'Gskill'),
(6, 'Kingmax'),
(5, 'Kingston'),
(3, 'MSI'),
(4, 'Server');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`id` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `customer` int(10) unsigned NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders_details`
--

CREATE TABLE IF NOT EXISTS `orders_details` (
`id` int(10) unsigned NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `product` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Triggers `orders_details`
--
DELIMITER //
CREATE TRIGGER `orders_details_after_delete_update_products` AFTER DELETE ON `orders_details`
 FOR EACH ROW UPDATE `products` SET `products`.`quantity` = `products`.`quantity` + OLD.`quantity` WHERE `products`.`id` = OLD.`product`
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `orders_details_after_insert_update_products` AFTER INSERT ON `orders_details`
 FOR EACH ROW UPDATE `products` SET `products`.`quantity` = `products`.`quantity` - NEW.`quantity` WHERE `products`.`id` = NEW.`product`
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `orders_details_after_update_update_products` AFTER UPDATE ON `orders_details`
 FOR EACH ROW UPDATE `products` SET `products`.`quantity` = `products`.`quantity` + OLD.`quantity` - NEW.`quantity` WHERE `products`.`id` = NEW.`product`
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `category` int(10) unsigned NOT NULL,
  `manufacturer` int(10) unsigned NOT NULL,
  `desc` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4294967295 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `quantity`, `category`, `manufacturer`, `desc`) VALUES
(1154347624, 'Kingmax 8GB Bus 1600 (1 x 8GB) DDR3L', 2080000, 16, 21, 6, ''),
(1154362778, 'Kingmax 2GB Bus 1600 DDR3L', 590000, 14, 21, 6, ''),
(1163741821, 'Gskill AEGIS Gaming 4GB (1x4GB) DDR3 Bus 1600 cas 9', 950000, 27, 22, 7, ''),
(1214362154, 'MSI B85-E45', 1770000, 10, 1, 3, ''),
(1285367113, 'MSI A78M-E35 FM2+', 1620000, 18, 1, 3, ''),
(1311648323, 'Gskill RIPJAWSX Gaming 8GB (1x8GB) DDR3 Bus 1600 Cas 9', 1950000, 10, 22, 7, ''),
(1365725743, 'Kingmax 4GB Bus 1600 (1 x 4GB) DDR3L', 1050000, 10, 21, 6, ''),
(1387562811, 'Kingston 2GB Bus 1600 9-9-9-24', 490000, 10, 22, 5, ''),
(1554241465, 'Gskill RIPJAWS Gaming 4GB (1x4GB) DDR3 Bus 1600 cas 9', 980000, 14, 22, 7, ''),
(1633328446, 'Kingston 4GB Bus 1600 9-9-9-24', 950000, 10, 22, 5, ''),
(1676634727, 'Kingtons Hyper X 4GB DDR3-1600 C9 KHX1600C9D3B1K2/8GX', 970000, 18, 22, 5, ''),
(1726366814, 'MSI B75MA-E33', 1395000, 14, 1, 3, ''),
(1833816152, 'MSI H81M-P33', 1350000, 18, 1, 3, ''),
(4294967295, 'MSI H81M-E33', 1365000, 10, 1, 3, '');

-- --------------------------------------------------------

--
-- Table structure for table `sale_off`
--

CREATE TABLE IF NOT EXISTS `sale_off` (
`id` int(10) unsigned NOT NULL,
  `product` int(10) unsigned NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `gender` bit(1) NOT NULL,
  `birthday` date NOT NULL,
  `id_number` char(9) COLLATE utf8_unicode_ci NOT NULL,
  `role` tinyint(4) NOT NULL,
  `cookie` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `address`, `phone`, `gender`, `birthday`, `id_number`, `role`, `cookie`) VALUES
(1, 'hminh1208@gmail.com', '123456', 'Phan Hồng Minh', '42 Trưng Nhị', '0934720607', b'1', '1993-06-14', '201620220', 1, NULL),
(2, 'dattran199@gmail.com', '123456', 'Trần Duy Đạt', '23 Ngô Gia Tự', '0935120393', b'1', '1993-03-12', '111111111', 2, NULL),
(3, 'trihung@gmail.com', '123456', 'Đinh Trí Hùng', '23 NGô Gia Tự', '0934720607', b'1', '1993-12-21', '222222222', 2, NULL),
(4, 'thanhhung@gmail.com', '123456', 'Trần Thanh Hùng', '42 Ngô Gia Tự', '0963542765', b'1', '1992-02-26', '333333', 2, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`), ADD KEY `parent` (`parent`);

--
-- Indexes for table `cate_manu`
--
ALTER TABLE `cate_manu`
 ADD PRIMARY KEY (`category`,`manufacturer`), ADD KEY `manufacturer` (`manufacturer`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `imports`
--
ALTER TABLE `imports`
 ADD PRIMARY KEY (`id`), ADD KEY `user` (`user`), ADD KEY `user_2` (`user`), ADD KEY `user_3` (`user`);

--
-- Indexes for table `imports_details`
--
ALTER TABLE `imports_details`
 ADD PRIMARY KEY (`id`), ADD KEY `import` (`import`,`product`), ADD KEY `import_2` (`import`), ADD KEY `product` (`product`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`id`), ADD KEY `user` (`user`), ADD KEY `customer` (`customer`);

--
-- Indexes for table `orders_details`
--
ALTER TABLE `orders_details`
 ADD PRIMARY KEY (`id`), ADD KEY `order` (`order`), ADD KEY `product` (`product`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`), ADD KEY `category` (`category`), ADD KEY `manufacturer` (`manufacturer`);

--
-- Indexes for table `sale_off`
--
ALTER TABLE `sale_off`
 ADD PRIMARY KEY (`id`), ADD KEY `product` (`product`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `id_number` (`id_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `imports`
--
ALTER TABLE `imports`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `imports_details`
--
ALTER TABLE `imports_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `manufacturers`
--
ALTER TABLE `manufacturers`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders_details`
--
ALTER TABLE `orders_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4294967295;
--
-- AUTO_INCREMENT for table `sale_off`
--
ALTER TABLE `sale_off`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
