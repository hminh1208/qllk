<!DOCTYPE html>
<html lang="en">

    <?php
    include_once './common.php';
    ?>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Hồng Minh Computer</title>

        <!-- Bootstrap Core CSS -->
        <link href="../assert/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../assert/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../assert/css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../assert/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- BootstrapValidator requires Bootstrap CSS v3.0.0 or higher -->
        <link rel="stylesheet" href="../assert/css/bootstrapValidator.css"/>

    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo $lang['LOGIN_PAGE_TITLE']; ?></h3>
                        </div>
                        <div class="panel-body">
                            <form class="loginForm" method="POST" action="../script/authScript.php">
                                <div class="form-group">
                                    <input class="form-control" placeholder="<?php echo $lang['EMAIL_PLACEHOLDER']; ?>" name="email" id="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="<?php echo $lang['PASSWORD_PLACEHOLDER']; ?>" name="password" id="password" type="password" >
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" id="remember" type="checkbox" ><?php echo $lang['REMEMBER_ME']; ?>
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" name="type" value ="login" class="btn btn-lg btn-success btn-block"><?php echo $lang['LOGIN_BUTTON']; ?></button>
                            </form>
                            <hr>
                                <button type="submit" class="btn btn-lg btn-primary btn-block" onclick=""><?php echo $lang['SIGNUP_BUTTON']; ?></button>
                                <button type="submit" class="btn btn-lg btn-danger btn-block" onclick=""><?php echo $lang['FORGOTPASS_BUTTON']; ?></button>          
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery Version 1.11.0 -->
        <script src="../assert/js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../assert/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../assert/js/plugins/metisMenu/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../assert/js/sb-admin-2.js"></script>

        <!-- and jQuery v1.9.1 or higher -->
        <script type="text/javascript" src="../assert/js/bootstrapValidator.js"></script>
        <script type="text/javascript" src="../assert/js/language/<?php echo $js_code ?>.js"></script>

        <!-- Custom Login JavaScript -->
        <script src="../assert/js/login/login.js"></script>

    </body>

</html>
