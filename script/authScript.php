<?php

header('Content-Type: text/html; charset=utf-8');

include './dbconnect.php';

if (isset($_POST['type'])) {
    if ($_POST['type'] == "login") {
        checkLogin($_POST['email'], $_POST['password']);
    }
}

function checkLogin($email, $password) {
    $flag = false;
    if (!($stmt = getConnect()->prepare("SELECT id,name FROM users WHERE email = (?) AND password = (?)"))) {
        echo "Prepare failed: (" . getConnect()->errno . ") " . getConnect()->error;
    } else {
        // bind params
        $stmt->bind_Param('ss', $email, $password);
        //check exists
        if (mysqli_stmt_execute($stmt)) {
            
            //get result
            mysqli_stmt_bind_result($stmt, $id, $name);
            
            //print result
            $row = mysqli_stmt_fetch($stmt);
                
            if($row == 1){
                session_start();
                $_SESSION['user_id'] = $id; 
                $_SESSION['user_name'] = $name; 
                $flag = true;               
            }
        }
        //close P.S
        mysqli_stmt_close($stmt);
    }
    
    if($flag){
        header("location: ../admin/masterpage.php");
    }else{
        header("location: login.php");
    }
    
}
