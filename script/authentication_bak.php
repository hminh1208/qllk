<?php // include_once './dbconnect.php';

if (is_ajax()) {
    if (isset($_POST["action"]) && !empty($_POST["action"])) { //Checks if action value exists
        $action = $_POST["action"];
        switch ($action) { //Switch case for value of action
            case "login": checkLogin($_POST["email"], $_POST["password"]);
                break;
        }
    }
}

//Function to check if the request is an AJAX request
function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

function checkLogin($email, $password) {
    if (!($stmt = getConnect()->prepare("SELECT * FROM users WHERE email = (?) AND password = (?)"))) {
        echo "Prepare failed: (" . getConnect()->errno . ") " . getConnect()->error;
    } else {
        $stmt->bind_Param('ss', $email, $password);
        if ($stmt->execute()) {
            $stmt->store_result();          
        }
    }
    $return = ['valid'=>false];
    if($stmt->num_rows == 1){
        $return = ['valid'=>true];
    }
    $return["json"] = json_encode($return);
    echo json_encode($return);
}
